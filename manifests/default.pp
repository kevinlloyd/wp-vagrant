
# https://github.com/sapienza/vagrant-php-box/blob/master/manifests/default.pp
Exec { path =>  [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ] }
# Package { ensure => installed, }

# http://stackoverflow.com/a/13655214
exec { 'apt-update': command => '/usr/bin/apt-get update' }
Exec['apt-update'] -> Package <| |>

class web {

  File {
    ensure  => file, owner => 'root', group => 'root', mode => '0644',
    require => Package['apache2'], notify => [Service['apache2'], Exec['apache-init'] ],
  }

  package { ['apache2', 'php5', 'libapache2-mod-php5', 'php5-mysql', 'phpmyadmin']: }
  file { '/etc/php5/apache2/php.ini': 
    source => '/vagrant/files/php.ini', backup => '.puppet-bak',
  }
  file { '/etc/apache2/sites-enabled/001-phpmyadmin':
    ensure  => link,
    target  => '/etc/phpmyadmin/apache.conf',
  }
  file { '/etc/apache2/sites-available/vagrant':
    source  => '/vagrant/files/vagrant.conf',
  }
  file { '/var/www': ensure => directory, }
  exec { 'apache-init': command => 'a2enmod rewrite; a2dissite default; a2ensite vagrant' } ~>
  service { 'apache2': ensure  => running, }
}

class mysql {
  package { ['mysql-server']: ensure => installed, } ->
  
  exec { 'set-mysql-password': 
    unless => "mysqladmin -uroot -p${mysql_password} status", 
    command => "mysqladmin -uroot password ${mysql_password}" 
  }

  define mysqldb( $user, $password ) { 
    exec { "create-${name}-db": 
      unless => "/usr/bin/mysql -u${user} -p${password} ${name}", 
      command => "/usr/bin/mysql -uroot -p${mysql_password} -e \"create database ${name}; grant all on ${name}.* to ${user}@localhost identified by '${password}';\"", 
      require => Exec['set-mysql-password'],
    } 
  }
}

# Install latest Wordpress - https://github.com/vagrantpress/vagrantpress/blob/master/puppet/modules/wordpress/manifests/init.pp
class wordpress::install {
  include mysql
  mysql::mysqldb { 'wordpress': user => 'wordpress', password => 'wordpress' }

  exec { 'download-wordpress': #tee hee
    command => 'wget http://wordpress.org/latest.tar.gz && tar xzvf latest.tar.gz', 
    cwd => '/var/www/', creates => '/var/www/wp-login.php' } ->
  exec { 'wordpress-clean':
    command => 'mv wordpress/* . && rmdir wordpress/ ; rm latest.tar.gz ; rm index.html', 
    cwd => '/var/www/', creates => '/var/www/wp-login.php'}
}

$mysql_password = 'root'
include web, wordpress::install
# Wordpress Vagrant (using Puppet)


## About
This has been moved from using shell to provision the vagrant machine to [Puppet](http://puppetlabs.com)

puppet is a lot more configurable.

## Instructions
* `git clone https://kevinlloyd@bitbucket.org/kevinlloyd/wp-vagrant.git`
* Check settings in default.pp (passwords, etc.)
